﻿Imports System.Text.RegularExpressions

Module Module1

    Sub Main()
        Console.Title = "Calculadora 1.2"
        Dim sentences As String = ""

        Console.WriteLine("")
        Console.WriteLine("")
        Console.WriteLine("")
        Console.WriteLine("			***********Bienvenido************")
        Console.WriteLine("			*                               *")
        Console.WriteLine("			*     Proyecto Calculadora      *")
        Console.WriteLine("			*                               *")
        Console.WriteLine("			*         Mario Subuyuj         *")
        Console.WriteLine("			*                               *")
        Console.WriteLine("			***************Beta**************")
        Console.WriteLine("")
        Console.WriteLine("")
        Console.WriteLine("			*********************************")
        Console.WriteLine("")
        Console.WriteLine("")
        Console.WriteLine("			 Ingrese una operacion  o 'Salir'")
        Console.WriteLine("")
        Console.WriteLine("")
        Console.WriteLine("			*********************************")



        Do
            sentences = Console.ReadLine()

            If sentences.ToLower() = "salir" Then Exit Do

            Try
                Console.WriteLine("Resultado: " & Operar(sentences))
            Catch ex As Exception
                Console.WriteLine("Error: " & ex.Message())
            End Try
        Loop
    End Sub

    Function Operar(ByVal sentences As String) As Double
        Dim respuesta As Double = 0D
        Dim temp As Double = 0D

        sentences = sentences.Replace(" ", "")
        sentences = sentences.Replace(".", ",")

        Dim RegexObj As New Regex _
            ("(?<Termino>[\+\-]?(?:(?:\d+(?:\,\d*)?|\([\d\,\+\-\/\*]*\))(?:[\*\/](?:\d+(?:\,\d*)?|\([\d\,\+\-\/\*]+\)))*))")


        If RegexObj.IsMatch(sentences) Then
            Dim MatchResults As MatchCollection = RegexObj.Matches(sentences)
            Dim MatchResult As Match = MatchResults(0)
            Dim termino As String


            For i As Int32 = 0 To MatchResults.Count - 1



                termino = MatchResult.Groups("Termino").Value
                If IsNumeric(termino) Then

                    temp = Double.Parse(termino)
                Else
                    Dim signo As Integer = 1

                    If termino.Substring(0, 1) = "-" Then
                        signo = -1
                        termino = termino.Substring(1)
                    ElseIf termino.Substring(0, 1) = "+" Then
                        signo = 1
                        termino = termino.Substring(1)
                    End If

                    temp = ResolverTermino(termino)

                    temp *= signo
                End If


                respuesta += temp
                MatchResult = MatchResult.NextMatch()
            Next

            Return respuesta
        Else
            Throw New FormatException("La operacion no pudo ser reconocida")
        End If

    End Function

    Private Function ResolverTermino(ByVal Termino As String) As Double
        Dim resp As Double = 0D
        Dim temp As Double = 0D

        Dim RegexObj As New Regex(
            "(?<Termino>[\+\-]? \( [\d\,\+\-\*\/\(\)]+ \)|[\*\/] (?: \( [\d\+\-\*\/\(\)]* \) | \d*\,?\d* )|[\+\-]? \d* (?:\,\d*)?)",
            RegexOptions.IgnorePatternWhitespace)

        If RegexObj.IsMatch(Termino) Then

            Dim MatchResults As MatchCollection = RegexObj.Matches(Termino)
            Dim MatchResult As Match = MatchResults(0)
            Dim subTermino As String
            For I As Int32 = 0 To MatchResults.Count - 2
                subTermino = MatchResult.Groups("Termino").Value

                If IsNumeric(subTermino) Then
                    resp = Double.Parse(subTermino)
                Else

                    If subTermino.Contains("^") Then
                        Dim regexPotencia As String = "^(?<Base>[+\-]?\d\,?\d*|[+\-]?\([+\-]?\d[\d,+*/\-\^]*\))\^(?<Potencia>[+\-]?\d\,?\d*|[+\-]?\([+\-]?\d[\d,+*/\-\^]*\))$"

                        Dim base As Double = Operar(Regex.Match(subTermino, regexPotencia).Groups("Base").Value)
                        Dim potencia As Double = Operar(Regex.Match(subTermino, regexPotencia).Groups("Potencia").Value)

                        resp = Math.Pow(base, potencia)
                    Else

                        Select Case subTermino.Substring(0, 1)
                            Case "*"
                                temp = Operar(subTermino.Substring(1))
                                resp *= temp
                            Case "/"
                                temp = Operar(subTermino.Substring(1))
                                resp /= temp
                            Case Else
                                resp = Operar(Regex.Match(subTermino, "\((?<Operacion>.*)\)").Groups("Operacion").Value)
                        End Select
                    End If
                End If
                MatchResult = MatchResult.NextMatch
            Next
            Return resp
        Else
            Throw New FormatException("Parte de la operacion no pudo ser reconocida")
        End If
    End Function

End Module

